const getSecondSectionPosition = () => {
    return document.querySelector('section:nth-of-type(2)').getBoundingClientRect().top;
}

const updateScrollArrowVisibility = () => {
    setScrollingArrowVisibility(getSecondSectionPosition() >= getViewportHeigh());
}

const setScrollingArrowVisibility = (shouldDisplay) => {
    document.querySelector('#scroll-down-icon').style.opacity = shouldDisplay ? 1 : 0;
}

window.addEventListener('scroll', updateScrollArrowVisibility);
window.addEventListener('rezise', updateScrollArrowVisibility);
